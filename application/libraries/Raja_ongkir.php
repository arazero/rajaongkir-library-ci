<?php

/**
 * RajaOngkir Library for CodeIgniter
 * Digunakan untuk mengkonsumsi API RajaOngkir dengan mudah
 * 
 * @author Andi Siswanto <andisis92@gmail.com>
 * 
 * Modified by Arazero <gramilust99@gmail.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'RajaOngkir/Endpoints.php';

class Raja_ongkir extends Endpoints {

    private $api_key;
    private $account_type;

    public function __construct() {
        // Pastikan bahwa PHP mendukung cURL
        if (!function_exists('curl_init')) {
            log_message('error', 'cURL Class - PHP was not built with cURL enabled. Rebuild PHP with --with-curl to use cURL.');
        }
        $this->ci = & get_instance();
        $this->ci->load->config('rajaongkir', TRUE);
        if ($this->ci->config->item('rajaongkir_api_key', 'rajaongkir') == "") {
            log_message("error", "Harap masukkan API KEY Anda di config.");
        } else {
            $this->api_key = $this->ci->config->item('rajaongkir_api_key', 'rajaongkir');
            $this->account_type = $this->ci->config->item('rajaongkir_account_type', 'rajaongkir');
        }
        parent::__construct($this->api_key, $this->account_type);
    }
}
