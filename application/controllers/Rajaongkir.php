<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rajaongkir extends CI_Controller {

  	// http://example.com/rajaongkir/province
	public function province(){
		$provinces = $this->raja_ongkir->province(); // output json
		$data = json_decode($provinces, true);
        echo json_encode($data['rajaongkir']['results']);
	}
  
  	// http://example.com/rajaongkir/city
  	public function city(){
		$cities = $this->raja_ongkir->city(); // output json
		//print_r($cities);
		$data = json_decode($cities, true);
        echo json_encode($data['rajaongkir']['results']);
	}
  
  	// http://example.com/rajaongkir/subdistrict
  	public function subdistrict(){
		$subdistrict = $this->raja_ongkir->subdistrict(151); // output json
	//print_r($subdistrict);
		$data = json_decode($subdistrict, true);
        echo json_encode($data['rajaongkir']['results']);
	}
  
  	// http://example.com/rajaongkir/cost
  		public function cost(){
		$cost = $this->raja_ongkir->cost(501, 114, 1000, "jne"); // output json
		print_r($cost);
		$data = json_decode($cost, true);
        echo json_encode($data['rajaongkir']['results']);
	}
  
}
